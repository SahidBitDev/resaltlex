defmodule Myregex do

  def run(sourceFile) do
    parse(sourceFile) |> initHTML |> inputHTML
  end

  def initHTML(string) do
    "<!DOCTYPE html>
    <html>
    <header>
      <title>Resaltador lexico para codigo Javascript (.js)</title>
      <h1>Resaltador lexico para codigo Javascript (.js)</h1>
      <h2>Hecho por: Sahid Emmanuel Rosas Maas -A01734211</h2>
      <h3>Codigo de: Evan You (Github user: yyx990803)</h4>
    </header>
    <body>
    <style>
        header{
          color: white;
        }
        body {
          background-color: #00000F;
        }
    </style>
    <pre><code>#{string}</code></pre>
    </body>
    </html>"
  end

  def inputHTML(myInput) do
    {:ok, file} = File.open("result.html", [:write])
    IO.binwrite(file, myInput)
    File.close(file)
  end
  
  def parse(myFile) do
  
    File.read!(myFile) |> to_charlist |> :lexer.string |> elem(1) |> Enum.map(fn {token,_,cl} -> 
    colorPicker(token,cl) end)
    |> Enum.join
  end

  def colorPicker(:string,cl) do
    "<span style=\"color:orange\">#{to_string(cl)}</span>"
  end
  def colorPicker(:num,cl) do
    "<span style=\"color:blue\">#{to_string(cl)}</span>"
  end
  def colorPicker(:comment,cl) do
    "<span style=\"color:green\">#{to_string(cl)}</span>"
  end
  def colorPicker(:operator,cl) do
    "<span style=\"color:yellow\">#{to_string(cl)}</span>"
  end
  def colorPicker(:keyword,cl) do
    "<span style=\"color:red\">#{to_string(cl)}</span>"
  end
  def colorPicker(:variable,cl) do
    "<span style=\"color:cyan\">#{to_string(cl)}</span>"
  end
  def colorPicker(:separator,cl) do
    "<span style=\"color:white\">#{to_string(cl)}</span>"
  end
  def colorPicker(:boolean,cl) do
    "<span style=\"color:white\">#{to_string(cl)}</span>"
  end
  def colorPicker(:null,cl) do
    "<span style=\"color:gray\">#{to_string(cl)}</span>"
  end
  def colorPicker(_any,cl) do
    to_string(cl)
  end
end