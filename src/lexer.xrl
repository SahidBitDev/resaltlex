Definitions.

Separator = \;|\,|\.|\:|\(|\)|\{|\}|\[|\]|\\
Operator = \+|\-|\=|\!|\*|\^|\~|\&|\||\<|\>|\/|\?
Num = [0-9]+([eE](-)?[0-9]+)?
Boolean = false|true
Float = {Num}+\.{Num}+([eE](-)?{Num}+)? 
Str = "[^\"]*"|'[^\']*'
Null = null|NaN
Infinity = Infinity
Hex = 0[xX][a-f0-9A-F]+
Octal = 0[oO]?[0-7]+
Binary = 0[bB][0-1]+
VariableName = [a-zA-Z_$][a-zA-Z0-9_$]*
Keyword = new|var|const|let|if|else|switch|for|while|try|catch|case|break|import|class|return|undefined|function|private|public|protected|static|debugger|goto|throw|yield|extends|in|do|async|await|package|interface|implements|with|instanceof
SingleLineComment = //.*
MultiLineComment = /\*[^(\*/)]*\*/
Skip = [\s\t\r\n]+

Rules.

{Skip} : {token,{space,TokenLine,TokenChars}}.
{Num} : {token,{num,TokenLine,TokenChars}}.
{Float} : {token,{num,TokenLine,TokenChars}}.
{Infinity} : {token,{num,TokenLine,TokenChars}}.
{Hex} : {token,{num,TokenLine,TokenChars}}.
{Octal} : {token,{num,TokenLine,TokenChars}}.
{Binary} : {token,{num,TokenLine,TokenChars}}.
{Str} : {token,{string,TokenLine,TokenChars}}.
{SingleLineComment} : {token,{comment,TokenLine,TokenChars}}.
{MultiLineComment} : {token,{comment,TokenLine,TokenChars}}.
{Boolean} : {token,{boolean,TokenLine,TokenChars}}.
{Keyword} : {token,{keyword,TokenLine,TokenChars}}.
{VariableName} : {token,{variable,TokenLine,TokenChars}}.
{Operator} : {token,{operator,TokenLine,TokenChars}}.
{Separator} : {token,{separator,TokenLine,TokenChars}}.
{Null} : {token,{null,TokenLine,TokenChars}}.

Erlang code.